import 'dart:convert';
import 'dart:async';
import 'package:any_time/VideoApp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:dio/dio.dart';
import 'package:carousel_slider/carousel_slider.dart';

import '../DetailsMoviePage.dart';


class TopWidget extends StatefulWidget {
  @override
  _TopWidgetState createState() => _TopWidgetState();
}

class _TopWidgetState extends State<TopWidget> {
  bool loading = true;
  List imageData = [];
  List imageDataVertical = [];
  List imageEnglish = [];
  List imageHindi = [];
  List imageSliders = [];
  int _current = 0;
  final CarouselController _controller = CarouselController();
  @override
  void initState() {
    super.initState();
    // Timer(Duration(seconds: 7), () {
    //   print('vrushabh completed 30 sec');
    //   setState(() {
    //     loading = false;
    //   });
    // });
    this.getHorizantal();
    this.getTrending();
    this.getEnglish();
    this.getHindi();
  }

  getHorizantal() async {
    try {
      var dio = Dio();
      final response = await dio.get(
          'http://api.etrackingpro.com/AnytimeService/AnytimeService.svc/get_campaigns?type=HCamp');
      //  print(response.data);

      var images = [];
      for (var i = 0; i < response.data.length; i++) {
        images.add(response.data[i]);
      }
      print(images);
      setState(() {
        imageData = images;
       
      });
    } catch (e) {
      print(e);
    }
  }

  getTrending() async {
    try {
      var dio = Dio();
      final response = await dio.get(
          'http://api.etrackingpro.com/AnytimeService/AnytimeService.svc/get_campaigns?type=VCamp');
      //  print(response.data);

      var images = [];
      for (var i = 0; i < response.data.length; i++) {
        images.add(response.data[i]);
      }
      print(images);
      setState(() {
        imageDataVertical = images;
         loading = false;
      });
    } catch (e) {
      print(e);
    }
  }

  getEnglish() async {
    try {
      var dio = Dio();
      final response = await dio.get(
          'http://api.etrackingpro.com/AnytimeService/AnytimeService.svc/get_campaigns?type=2&subtype=All');
      //  print(response.data);

      var images = [];
      for (var i = 0; i < response.data.length; i++) {
        images.add(response.data[i]);
      }
      print(images);
      setState(() {
        imageEnglish= images;
         loading = false;
      });
    } catch (e) {
      print(e);
    }
  }

  getHindi() async {
    try {
      var dio = Dio();
      final response = await dio.get(
          'http://api.etrackingpro.com/AnytimeService/AnytimeService.svc/get_campaigns?type=3&subtype=All');
      //  print(response.data);

      var images = [];
      for (var i = 0; i < response.data.length; i++) {
        images.add(response.data[i]);
      }
      print(images);
      setState(() {
        imageHindi = images;
         loading = false;
      });
    } catch (e) {
      print(e);
    }
  }

  PageController _pageController =
      PageController(initialPage: 1, viewportFraction: 0.8);

  var data = json.decode(
      '[{"campaign_id":"20","name":"Khuda Haafiz","image_url":"http://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg","update_counter":"0","price":"10","currency":"INR","ColourCode":null,"description":"Drishyam ( transl. Visuals) is a 2015 Indian Hindi-language thriller film directed by Nishikant Kamat. The film is a remake of the 2013 Malayalam-language film of the same name. The film was jointly produced by Kumar Mangat Pathak, Ajit Andhare and Abhishek Pathak" ,"progress":"0.8"},{"campaign_id":"13","name":"Drishyam","image_url":"http://anytimebucket.s3.ap-south-1.amazonaws.com/images/HorizontalImages/Dri1.jpg","update_counter":"0","price":"199","currency":"INR","ColourCode":null,"description":"Khuda Haafiz is a 2020 Indian Hindi-language action thriller film written and directed by Faruk Kabir and produced by Kumar Mangat Pathak and Abhishek Pathak under Panorama Studios. It stars Vidyut Jammwal and Shivaleeka Oberoi, with Annu Kapoor, Aahana Kumra and Shiv Panditt in supporting roles.","progress":"0.4"},{"campaign_id":"12","name":"Thor Ragnarok","image_url":"http://cdn.pixabay.com/photo/2016/11/08/05/26/woman-1807533_960_720.jpg","update_counter":"0","price":"199","currency":"INR","ColourCode":null,"description":"Imprisoned on the planet Sakaar, Thor must race against time to return to Asgard and stop Ragnarok, the destruction of his world, at the hands of the powerful and ruthless villain Hela.","progress":"0.6"},{"campaign_id":"2","name":"Iron Man 3","image_url":"http://cdn.pixabay.com/photo/2016/02/13/12/26/aurora-1197753_960_720.jpg","update_counter":"1","price":"199","currency":"USD","ColourCode":null,"description":"Marvels Iron Man 3 pits brash-but-brilliant industrialist Tony Stark\/Iron Man against an enemy whose reach knows no bounds. When Stark finds his personal world destroyed at his enemys hands, he embarks on a harrowing quest to find those responsible. This journey, at every turn, will test his mettle","progress":"0.8"},{"campaign_id":"8","name":"Laxmii","image_url":"http://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228_960_720.jpg","update_counter":"1","price":"99","currency":"INR","ColourCode":null,"description":"Laxmii is a 2020 Indian Hindi-language comedy horror film written and directed by Raghava Lawrence, marking his directorial debut in Hindi cinema. ... The film revolves around a man who gets possessed by the ghost of a transgender person.","progress":"0.8"}]');

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: loading == true
            ? Container(
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Column(
                  children: [
                  
                    CarouselSlider(
                      items: imageSliders = imageData
                          .map((item) => Container(
                                // color: Colors.red,
                                width: 300,
                                child: Container(
                                  margin: EdgeInsets.all(2.0),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.0)),
                                      child: Stack(
                                        children: <Widget>[
                                          Image.network(
                                            item['image_url'],
                                            fit: BoxFit.cover,
                                          ),
                                          Positioned(
                                            bottom: 0.0,
                                            left: 0.0,
                                            right: 0.0,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                  colors: [
                                                    Color.fromARGB(50, 0, 0, 0)
                                                        .withOpacity(0.6),
                                                    Color.fromARGB(0, 0, 0, 0)
                                                  ],
                                                  begin: Alignment.bottomCenter,
                                                  end: Alignment.topCenter,
                                                ),
                                              ),
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 5.0,
                                                  horizontal: 20.0),
                                              child: Column(
                                                children: [
                                                  Text(
                                                    item['name'],
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 20.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                    '2018 | Crime | Drama |Thriller |',
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 13.0,
                                                    ),
                                                  ),
                                                  Positioned.fill(
                                                    // bottom: 90.0,
                                                    child: Align(
                                                      alignment: Alignment
                                                          .bottomCenter,
                                                      child: RawMaterialButton(
                                                        padding:
                                                            EdgeInsets.all(8.0),
                                                        elevation: 12.0,
                                                        onPressed: () {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder: (_) =>
                                                                  VideoApp(),
                                                            ),
                                                          );
                                                        },
                                                        shape: CircleBorder(),
                                                        fillColor: Colors
                                                            .lightBlue[300],
                                                        child: Icon(
                                                          Icons.play_arrow,
                                                          size: 20.0,
                                                          color: Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      )),
                                ),
                              ))
                          .toList(),
                      carouselController: _controller,
                      options: CarouselOptions(
                          height: 190,
                          autoPlay: false,
                          viewportFraction: 0.9,
                          enlargeCenterPage: true,
                          // aspectRatio: 2.0,
                          // aspectRatio: 16/9,
                          onPageChanged: (index, reason) {
                            setState(() {
                              _current = index;
                            });
                          }),
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: imageData.asMap().entries.map((entry) {
                          return GestureDetector(
                            onTap: () => _controller.animateToPage(entry.key),
                            child: Container(
                              width: 8.0,
                              height: 8.0,
                              margin: EdgeInsets.symmetric(
                                  vertical: 8.0, horizontal: 4.0),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: (Colors.lightBlueAccent[100])
                                      .withOpacity(
                                          _current == entry.key ? 0.9 : 0.4)),
                            ),
                          );
                        }).toList()),

                    Column(
                      children: [
                        Container(
                            alignment: Alignment.bottomLeft,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(15, 0, 0, 0),
                              child: Text(
                                'Countinue Watching',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 17),
                              ),
                            )),
                        Container(
                            margin: EdgeInsets.symmetric(vertical: 20.0),
                            height: 170.0,
                            // color: Colors.red,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: data.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(15, 0, 3, 0),
                                    child: Column(
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            // color: Colors.red,
                                            image: DecorationImage(
                                                image: NetworkImage(
                                                    data[index]['image_url']),
                                                fit: BoxFit.cover),
                                          ),
                                          height: 150,
                                          width: 100,
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: [
                                            Container(
                                              color: Colors.yellow,
                                              height: 1,
                                              width: 95,
                                              child: LinearProgressIndicator(
                                                value: double.parse(
                                                    data[index]['progress']),
                                                valueColor:
                                                    AlwaysStoppedAnimation<
                                                            Color>(
                                                        Color(0xff1794d3)),
                                                backgroundColor:
                                                    Color(0xffD6D6D6),
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  );
                                })),
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                            alignment: Alignment.bottomLeft,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(15, 0, 0, 0),
                              child: Text(
                                'Trending Now',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 17),
                              ),
                            )),
                        Container(
                            margin: EdgeInsets.symmetric(vertical: 20.0),
                            height: 150.0,
                            //color: Colors.red,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: imageDataVertical.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(15, 0, 3, 0),
                                    child: Column(
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            // color: Colors.red,
                                            image: DecorationImage(
                                                image: NetworkImage(
                                                    imageDataVertical[index]['image_url']),
                                                fit: BoxFit.cover),
                                          ),
                                          height: 150,
                                          width: 100,
                                        ),
                                      ],
                                    ),
                                  );
                                })),
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                            alignment: Alignment.bottomLeft,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(15, 0, 0, 0),
                              child: Text(
                                'English Movies',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 17),
                              ),
                            )),
                        Container(
                            margin: EdgeInsets.symmetric(vertical: 20.0),
                            height: 150.0,
                            //color: Colors.red,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: imageEnglish.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(15, 0, 3, 0),
                                    child: Column(
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            // color: Colors.red,
                                            image: DecorationImage(
                                                image: NetworkImage(
                                                    imageEnglish[index]['image_url']),
                                                fit: BoxFit.cover),
                                          ),
                                          height: 150,
                                          width: 100,
                                        ),
                                      ],
                                    ),
                                  );
                                })),
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                            alignment: Alignment.bottomLeft,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(15, 0, 0, 0),
                              child: Text(
                                'Hindi Movies',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 17),
                              ),
                            )),
                        Container(
                            margin: EdgeInsets.symmetric(vertical: 20.0),
                            height: 150.0,
                            //color: Colors.red,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: imageHindi.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(15, 0, 3, 0),
                                    child: Column(
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            //color: Colors.red,
                                            image: DecorationImage(
                                                image: NetworkImage(
                                                    imageHindi[index]['image_url']),
                                                fit: BoxFit.cover),
                                          ),
                                          height: 150,
                                          width: 100,
                                        ),
                                      ],
                                    ),
                                  );
                                })),
                      ],
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  slider() {
    return (ListView.builder(
        itemCount: imageData.length,
        itemBuilder: (BuildContext context, int index) {
          return (Container(
            child: Image.network(imageData[index]),
          ));
        }));
  }

  _itemHeadMovie(BuildContext context, int index) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (BuildContext context, Widget widget) {
        double value = 1;
        if (_pageController.position.haveDimensions) {
          value = _pageController.page - index;
          value = (1 - (value.abs() * 0.3) + 0.06).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 270.0,
            width: Curves.easeInOut.transform(value) * 400.0,
            child: widget,
          ),
        );
      },
      child: GestureDetector(
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => DetailsMoviePage(
              movie1: data[index],
            ),
          ),
        ),
        child: Stack(
          children: <Widget>[
            Center(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black54,
                      offset: Offset(0.0, 4.0),
                      blurRadius: 10.0,
                    ),
                  ],
                ),
                child: Center(
                  child: Hero(
                    tag: data[index]['name'],
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: FadeInImage.assetNetwork(
                        placeholder: 'assets/placeholder.png',
                        height: 220.0,
                        width: double.infinity,
                        fit: BoxFit.cover,
                        image: data[index]['image_url'],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              left: 30.0,
              bottom: 40.0,
              child: Align(
                child: Container(
                  width: 250.0,
                  child: Text(
                    data[index]['name'],
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getHorizontalCamp() async {
    // setState(() {
    //   loadingData = true;
    // });

    var response = await get(
        Uri.parse(
            'http://api.etrackingpro.com/AnytimeService/AnytimeService.svc/get_campaigns?type=VCamp'),
        headers: {'Content-Type': 'application/json', 'Charset': 'utf-8'});

    var data = json.decode(response.body);

    print(data);

    // setState(() {
    //   loadingData = false;
    // });
  }
}
