import 'dart:convert';

import 'package:any_time/VideoApp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:video_player/video_player.dart';
import 'Other/CircularClipper.dart';

class DetailsMoviePage extends StatefulWidget {
  var movie = json.decode(
      '{"Director_name":"Nishikant Kamat","Download":"","Favourite":"","Film_ind_name":"Bollywood","FiveRating":"0","FourRating":"0","Genre_Name":"Action","ISsurv":"0","Language_name":"Hindi","Like_Dislike":"","MPAA_Name":"G","OneRating":"0","Producer_name":"Sarita Patil","RatingAvg":"1.50","Release_date":"13 Dec 2020","ThreeRating":"1","TwoRating":"0","UserRating":"0.0","casts":"Ajay Devgn, Shriya Saran, Tabu, Rajat Kapoor,Ishita Dutta","currency":"INR","description":"Drishyam ( transl. Visuals) is a 2015 Indian Hindi-language thriller film directed by Nishikant Kamat. The film is a remake of the 2013 Malayalam-language film of the same name. The film was jointly produced by Kumar Mangat Pathak, Ajit Andhare and Abhishek Pathak","h_image_url":"https:\/\/anytimebucket.s3.ap-south-1.amazonaws.com\/images\/HorizontalImages\/Dri1.jpg","name":"Drishyam","price":"10","synopsis":"A 2015 Indian Hindi-language","type":"1","v_image_url":"https:\/\/anytimebucket.s3.ap-south-1.amazonaws.com\/images\/VerticalImages\/Dri2.jpg","video_url":"https:\/\/anytimebucket.s3.ap-south-1.amazonaws.com\/Videos\/videoplayback.mp4"}');

  var movie1;

  DetailsMoviePage({Key key, this.movie1}) : super(key: key);

  @override
  _DetailsMoviePageState createState() => _DetailsMoviePageState();
}

class _DetailsMoviePageState extends State<DetailsMoviePage> {
  VideoPlayerController _controller;
  var data = json.decode(
      '[{"campaign_id":"20","name":"Khuda Haafiz","image_url":"http://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg","update_counter":"0","price":"10","currency":"INR","ColourCode":null,"description":"Drishyam ( transl. Visuals) is a 2015 Indian Hindi-language thriller film directed by Nishikant Kamat. The film is a remake of the 2013 Malayalam-language film of the same name. The film was jointly produced by Kumar Mangat Pathak, Ajit Andhare and Abhishek Pathak" ,"progress":"0.8"},{"campaign_id":"13","name":"Drishyam","image_url":"http://anytimebucket.s3.ap-south-1.amazonaws.com/images/HorizontalImages/Dri1.jpg","update_counter":"0","price":"199","currency":"INR","ColourCode":null,"description":"Khuda Haafiz is a 2020 Indian Hindi-language action thriller film written and directed by Faruk Kabir and produced by Kumar Mangat Pathak and Abhishek Pathak under Panorama Studios. It stars Vidyut Jammwal and Shivaleeka Oberoi, with Annu Kapoor, Aahana Kumra and Shiv Panditt in supporting roles.","progress":"0.4"},{"campaign_id":"12","name":"Thor Ragnarok","image_url":"http://cdn.pixabay.com/photo/2016/11/08/05/26/woman-1807533_960_720.jpg","update_counter":"0","price":"199","currency":"INR","ColourCode":null,"description":"Imprisoned on the planet Sakaar, Thor must race against time to return to Asgard and stop Ragnarok, the destruction of his world, at the hands of the powerful and ruthless villain Hela.","progress":"0.6"},{"campaign_id":"2","name":"Iron Man 3","image_url":"http://cdn.pixabay.com/photo/2016/02/13/12/26/aurora-1197753_960_720.jpg","update_counter":"1","price":"199","currency":"USD","ColourCode":null,"description":"Marvels Iron Man 3 pits brash-but-brilliant industrialist Tony Stark\/Iron Man against an enemy whose reach knows no bounds. When Stark finds his personal world destroyed at his enemys hands, he embarks on a harrowing quest to find those responsible. This journey, at every turn, will test his mettle","progress":"0.8"},{"campaign_id":"8","name":"Laxmii","image_url":"http://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228_960_720.jpg","update_counter":"1","price":"99","currency":"INR","ColourCode":null,"description":"Laxmii is a 2020 Indian Hindi-language comedy horror film written and directed by Raghava Lawrence, marking his directorial debut in Hindi cinema. ... The film revolves around a man who gets possessed by the ghost of a transgender person.","progress":"0.8"}]');

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    _controller = VideoPlayerController.network(widget.movie['video_url'])
      ..initialize().then((_) {});
  }

  @override
  void dispose() {
    _controller.pause();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: Center(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: _controller.value.isPlaying
                ? Stack(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            _controller.pause();
                            _controller = VideoPlayerController.network(
                                widget.movie['video_url'])
                              ..initialize().then((_) {});
                          });
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          child: AspectRatio(
                            aspectRatio: _controller.value.aspectRatio,
                            child: VideoPlayer(_controller),
                          ),
                        ),
                      )
                    ],
                  )
                : ListView(
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Container(
                            child: FadeInImage.assetNetwork(
                              placeholder: 'assets/images/placeholder.png',
                              height: 250.0,
                              width: double.infinity,
                              fit: BoxFit.cover,
                              image: widget.movie['h_image_url'],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              IconButton(
                                // padding: EdgeInsets.only(left: 30.0),
                                onPressed: () => Navigator.pop(context),
                                icon: Icon(Icons.menu),
                                iconSize: 30.0,
                                color: Colors.white,
                              ),
                              IconButton(
                                // padding: EdgeInsets.only(left: 30.0),
                                onPressed: () => {},
                                icon: Icon(Icons.person_outline),
                                iconSize: 30.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                          Positioned.fill(
                            bottom: 90.0,
                            child: Align(
                              alignment: Alignment.bottomCenter,
                              child: RawMaterialButton(
                                padding: EdgeInsets.all(10.0),
                                elevation: 12.0,
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) => VideoApp(),
                                    ),
                                  );
                                },
                                shape: CircleBorder(),
                                fillColor: Colors.white.withOpacity(0.2),
                                child: Icon(
                                  Icons.play_arrow,
                                  size: 40.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SingleChildScrollView(
                        child: Column(
                          children: [
                            Column(
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(15, 10, 15, 0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          height: 55,
                                          // color: Colors.red,
                                          child: Column(
                                            children: [
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  widget.movie['name'],
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 20.0,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Container(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Text(
                                                    '2009 | Crime | Drama | Thriller',
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  )),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          alignment: Alignment.centerRight,
                                          height: 55,
                                          // color: Colors.red,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                0, 5, 0, 0),
                                            child: Column(
                                              children: [
                                                Container(
                                                  alignment:
                                                      Alignment.centerRight,
                                                  child: Text(
                                                    '2h 28 min',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 13),
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Container(
                                                    alignment:
                                                        Alignment.centerRight,
                                                    child: Text(
                                                      '18+',
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 13),
                                                    )),
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(15, 0, 15, 0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                          flex: 3,
                                          child: RaisedButton(
                                            onPressed: () {},
                                            child: Text('Play'),
                                          )),
                                      Expanded(child: Text('')),
                                      Expanded(
                                          flex: 3,
                                          child: RaisedButton(
                                            onPressed: () {},
                                            child: Text('Download'),
                                          )),
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Text(
                                      widget.movie['description'],
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        15, 0, 15, 10),
                                    child: Text(
                                      'Director : ' +
                                          widget.movie['Director_name'],
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        15, 0, 15, 10),
                                    child: Text(
                                      'Starring : Ajay Devgan , Tabbu',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(15, 0, 15, 0),
                                  child: Row(
                                    children: [
                                     
                                      Expanded(
                                        flex: 2,
                                        child: Container(
                                          child: RaisedButton(
                                            onPressed: () {},
                                            child: Icon(
                                              Icons.add,
                                              color: Colors.black,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          child: Text(''),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Container(
                                          child: RaisedButton(
                                            onPressed: () {},
                                            child: Icon(
                                              Icons.thumb_up,
                                              color: Colors.black,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          child: Text(''),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Container(
                                          child: RaisedButton(
                                            onPressed: () {},
                                            child: Icon(
                                              Icons.thumb_down,
                                              color: Colors.black,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          child: Text(''),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Container(
                                          child: RaisedButton(
                                            onPressed: () {},
                                            child: Icon(
                                              Icons.share,
                                              color: Colors.black,
                                            ),
                                          ),
                                        ),
                                      ),
                                     
                                    ],
                                  ),
                                ),
                                SizedBox(height: 15),
                                Column(
                                  children: [
                                    Container(
                                        alignment: Alignment.bottomLeft,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              15, 0, 0, 0),
                                          child: Text(
                                            'More Like This',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17),
                                          ),
                                        )),
                                    Container(
                                        margin: EdgeInsets.symmetric(
                                            vertical: 20.0),
                                        height: 170.0,
                                        // color: Colors.red,
                                        child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            itemCount: data.length,
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              return Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        15, 0, 3, 0),
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10.0),
                                                        // color: Colors.red,
                                                        image: DecorationImage(
                                                            image: NetworkImage(
                                                                data[index][
                                                                    'image_url']),
                                                            fit: BoxFit.cover),
                                                      ),
                                                      height: 150,
                                                      width: 100,
                                                    ),
                                                  ],
                                                ),
                                              );
                                            })),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      )

                      // Padding(
                      //   padding: EdgeInsets.symmetric(
                      //       horizontal: 40.0, vertical: 20.0),
                      //   child: Column(
                      //     crossAxisAlignment: CrossAxisAlignment.center,
                      //     children: <Widget>[
                      //       Text(
                      //         widget.movie['name'].toUpperCase(),
                      //         style: TextStyle(
                      //           color: Colors.blueAccent,
                      //           fontSize: 20.0,
                      //           fontWeight: FontWeight.bold,
                      //         ),
                      //         textAlign: TextAlign.center,
                      //       ),
                      //       SizedBox(height: 10.0),
                      //       Text(
                      //         widget.movie['Genre_Name'] + ' Film By',
                      //         style: TextStyle(
                      //           color: Colors.black54,
                      //           fontSize: 16.0,
                      //         ),
                      //       ),
                      //       SizedBox(height: 5.0),
                      //       Text(
                      //         widget.movie['Director_name'],
                      //         style: TextStyle(
                      //           color: Colors.black,
                      //           fontSize: 16.0,
                      //           fontWeight: FontWeight.w600,
                      //         ),
                      //       ),
                      //       SizedBox(height: 15.0),
                      //       Row(
                      //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      //         children: <Widget>[
                      //           Column(
                      //             children: <Widget>[
                      //               Text(
                      //                 'Release date',
                      //                 style: TextStyle(
                      //                   color: Colors.black54,
                      //                   fontSize: 16.0,
                      //                 ),
                      //               ),
                      //               SizedBox(height: 2.0),
                      //               Text(
                      //                 widget.movie['Release_date'].toString(),
                      //                 style: TextStyle(
                      //                   fontSize: 20.0,
                      //                   fontWeight: FontWeight.w600,
                      //                 ),
                      //               ),
                      //             ],
                      //           ),
                      //           Column(
                      //             children: <Widget>[
                      //               Text(
                      //                 widget.movie['Language_name'],
                      //                 style: TextStyle(
                      //                   color: Colors.black54,
                      //                   fontSize: 16.0,
                      //                 ),
                      //               ),
                      //               SizedBox(height: 2.0),
                      //               Text(
                      //                 widget.movie['Film_ind_name'].toUpperCase(),
                      //                 style: TextStyle(
                      //                   fontSize: 20.0,
                      //                   fontWeight: FontWeight.w600,
                      //                 ),
                      //               ),
                      //             ],
                      //           ),
                      //         ],
                      //       ),
                      //       SizedBox(height: 25.0),
                      //       Container(
                      //         height: 120.0,
                      //         child: SingleChildScrollView(
                      //           child: Text(
                      //             widget.movie['description'],
                      //             style: TextStyle(
                      //               color: Colors.black54,
                      //             ),
                      //           ),
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      // VerticalMovieWidget(title: 'Screen', movie: widget.movie)
                    ],
                  ),
          ),
        ));
  }
}
