import 'package:any_time/home/TopWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int tabIndex = 0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: Colors.black,
          appBar: getAppBar(),
          body: TabBarView(
            children: [
              TopWidget(),
              Container(
                width: double.infinity,
                height: double.infinity,
                alignment: Alignment.center,
                child: Text('Under Developement',style: TextStyle(color: Colors.white,fontSize: 25),),
              ),
               Container(
                width: double.infinity,
                height: double.infinity,
                alignment: Alignment.center,
                child: Text('Under Developement',style: TextStyle(color: Colors.white,fontSize: 25),),
              ),
            ],
          ),
          bottomNavigationBar: new TabBar(
            tabs: [
              Tab(
                icon: new Icon(Icons.home),
                text: 'Home',
               
              ),
              Tab(
                icon: new Icon(Icons.content_copy),
                text: 'Coming Soon',
              ),
              Tab(
                icon: new Icon(Icons.cloud_download),
                text: 'Download',
                
              ),
            ],
            labelColor: Colors.white,
            labelStyle: TextStyle(fontSize: 12),
            unselectedLabelColor: Colors.grey[800],
            unselectedLabelStyle: TextStyle(fontSize: 12),
            indicatorSize: TabBarIndicatorSize.label,
            indicatorColor: Colors.white,

          ),

          // ListView(children: <Widget>[
          //   TopWidget(),
          //   Container(
          //     child: Text(
          //       'hii',
          //       style: TextStyle(color: Colors.white, fontSize: 20),
          //     ),
          //   ),
          // ]),
          // bottomNavigationBar: BottomNavigationBar(
          //     selectedItemColor: Colors.white,
          //     unselectedItemColor: Colors.grey[800],
          //     backgroundColor: Colors.black,
          //     currentIndex: tabIndex,
          //     // selectedItemColor: Colors.red,
          //     //selectedLabelStyle: TextStyle(textBaseline: ),
          //     onTap: (int index) {
          //       setState(() {
          //         tabIndex = index;
          //       });
          //     },
          //     items: [
          //       BottomNavigationBarItem(
          //         icon: Icon(Icons.home),
          //         title: Text(
          //           'Home',
          //         ),
          //       ),
          //       BottomNavigationBarItem(
          //         icon: Icon(Icons.content_copy),
          //         title: Text('Coming Soon'),
          //       ),
          //       BottomNavigationBarItem(
          //         icon: Icon(Icons.cloud_download),
          //         title: Text('Downloads'),
          //       ),
          //     ]),
        ),
      ),
    );
  }

  getAppBar() => AppBar(
          backgroundColor: Colors.black,
          elevation: 0.0,
          title: Center(
            child: Image(
              alignment: Alignment.center,
              height: 25,
              image: AssetImage('assets/app_logo.png'),
            ),
          ),
          leading: IconButton(
              padding: EdgeInsets.only(left: 10.0),
              onPressed: () => print('Menu'),
              icon: Icon(Icons.menu),
              iconSize: 30.0,
              color: Colors.white),
          actions: [
            IconButton(
                padding: EdgeInsets.only(right: 10.0),
                onPressed: () => print('Search'),
                icon: Icon(Icons.search_rounded),
                iconSize: 30.0,
                color: Colors.white)
          ]);
}
